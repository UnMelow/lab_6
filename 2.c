#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

int total_procs = 0;
int N;

void copyFile(char* src, char *dst, mode_t mode)
{
    pid_t child;
    int status;

    while (total_procs >= N) {
        wait(&status);
        total_procs--;
    }

    if ( (child = fork()) == 0 ) {
        int f1 = open(src, O_RDONLY);
        int f2 = open(dst, O_WRONLY|O_CREAT, mode);
        char buf[1024];
        int r;
        int total_copied = 0;
        while ( (r = read(f1, buf, 1024)) > 0) {
            write(f2, buf, r);
            total_copied += r;
            if (r<1024) {
                break;
            }
        }
        printf("Process %d, file %s, copied %d bytes\n", getpid(), src, total_copied);
    }
    else {
        total_procs++;
    }
}

void cmp_folders(char *path, char *dir1, char *dir2)
{
    DIR *d = opendir(path);
    if (d == NULL) {
        return;
    }

    struct dirent *dir;
    struct stat st1, st2;
    while ((dir = readdir(d)) != NULL) {
        char d_path[4096];
        snprintf(d_path, 4096, "%s/%s", path, dir->d_name);
        char d_path2[4096];
        snprintf(d_path2, 4096, "%s", dir2);
        int idx = strlen(dir2);
        for (int i=strlen(dir1); i < strlen(d_path); i++) {
             d_path2[idx++] = d_path[i];
        }
        d_path2[idx] = '\0';
        int exists = stat(d_path2, &st2);
        stat(d_path, &st1);

        if(dir-> d_type != DT_DIR && exists == -1) {
            copyFile(d_path, d_path2, st1.st_mode);
        }
        else if(dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0 )
        {
            if (exists == -1) {
                mkdir(d_path2, st1.st_mode);
            }
            cmp_folders(d_path, dir1, dir2);
        }
    }
    closedir(d);
}

int main(int argc, char **argv)
{
    N = atoi(argv[3]);
    cmp_folders(argv[1], argv[1], argv[2]);

    return 0;
}
